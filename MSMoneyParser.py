#!/usr/local/bin

import numpy as np

import sys

"""

Ben Rotter - September 2016

BMO Harris Bank, where most of my transactions are done, only allows you to save their transaction data in stupid types.
.csv  - what you would want, but they don't include the description field so it is useless!
.ofx  - some weird Microsoft Money format that is easy to parse (html style fields) but isn't directly importable into a spreadshee
other - some super obsfucated quicken crap

So this is a python parser that should read in the .ofx file and output a USEFUL .csv file (with the descriptions)

Works for: BMO Harris, First Hawaiian Bank.

Doesn't work for the BOH MSMoney "active statement" though because they have a weird non-newline separated format

"""


def main(fileName,fileNameOut=-1):

    if fileNameOut == -1:
        fileNameOut = fileName.split("/")[-1].split(".")[0]+".csv"

    transactions = parseFile(importFile(fileName))
    
    printTransactions(transactions)

    writeTransactionsToFile(transactions,fileNameOut)

    return

def importFile(fileName):
    """
    Import the file: just a wrapper for the loadtxt function
    """
    file = np.loadtxt(fileName,dtype=str,delimiter=">",skiprows=11)

    return file

def parseFile(file):
    """

    the html format is okay to parse if you cut on ">", because then the first field is a shorthand description and the second field is the value!

    We are interested in the following fields:
    DTPOSTED - the date it was posted (%Y%M%D%H*%?)
    TRNAMT   - the amount! (with useful sign!)
    NAME     - the charging institution
    Memo     - Usually pointless (things like ACH DEBIT or POS PURCHASE W PIN)

    A transaction begins with a <STMTTRN> and ends with </STMTTRN>
    """


    transactions = []
    inTransaction = False
    for line in file:
        descrip = line[0]
        value   = line[1]

        if "/STMTTRN" in descrip:
            inTransaction = False
        elif "STMTTRN" in descrip:
            if "STMTTRNRS" in descrip:
                continue #this is some other thing that I don't want
            transactions.append({})
            inTransaction = True
        elif inTransaction:
            if "TRNAMT" in descrip:
                transactions[-1]["amount"] = value
            elif "DTPOSTED" in descrip:
                transactions[-1]["year"] = value[0:4]
                transactions[-1]["day"] = value[4:6]
                transactions[-1]["month"] = value[6:8]
                transactions[-1]["hour"] = value[8:10]
            elif "NAME" in descrip:
                transactions[-1]["name"] = value
            elif "MEMO" in descrip:
                transactions[-1]["memo"] = value

    return transactions


            
            
def printTransactions(transactions):

    for trans in transactions:
        try:
            print trans["day"],trans["month"],trans["year"]," - ",trans["amount"],trans["name"]
        except:
            print "BROKEN",trans

    return

def writeTransactionsToFile(transactions,fileOutName):
    
    file = open(fileOutName,"w")

    for trans in transactions:
        try:
            file.write(trans["day"]+"/"+trans["month"]+"/"+trans["year"]+","+trans["name"]+","+trans["memo"]+","+trans["amount"]+"\n")
        except:
            try:
                file.write(trans["day"]+"/"+trans["month"]+"/"+trans["year"]+","+trans["name"]+",,"+trans["amount"]+"\n")
            except:
                print "BROKEN",trans

    file.close()
            
    print "Wrote to file "+fileOutName

    return


if __name__ == "__main__":
    print "Running"
    if len(sys.argv) == 2:
        main(sys.argv[1])
    elif len(sys.argv) == 3:
        main(sys.argv[1],fileNameOut=sys.argv[2]+".csv")
    else:
        print "Usage: python "+sys.argv[0]+" [input .ofx file name] [optional: output base name]"
